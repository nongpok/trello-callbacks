const callback1 = require('./callback1');
const callback2 = require('./callback2');
const callback3 = require('./callback3');


function callback4(boardsData, listsData, cardsData, boardName, listName, callback){

    setTimeout(function(){

        const boardData = boardsData.find(function(board){
            return boardName === board.name;
        });

        if(boardData === undefined){
            callback(new Error('No board data for the given board name', null));
        }else{

            callback1(boardsData, boardData.id, function(err, board){
                if(err){
                    console.log(err);
                }else{
                    console.log(board);

                    callback2(listsData, boardData.id, function(err, list){
                        if(err){
                            console.log(err);
                        }else{
                            console.log(list);

                            const listData = list.find(function(data) {
                                return listName === data.name;
                            });

                            if(listData === undefined){
                                callback(new Error('No list data for the given list name found'), null);
                            }else{

                                callback3(cardsData, listData.id, function(err, card){
                                    if(err){
                                        console.log(err);
                                    }else{
                                        console.log(card);
                                    }
                                });
                            }

                        }
                    })
                }
            });
        }

    }, 2000);
}



module.exports = callback4;