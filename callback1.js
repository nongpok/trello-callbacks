
function callback1(boardsData, boardID, callback){

    setTimeout(function(){

        const getBoardID = boardsData.find(function(board){
            return board.id === boardID;
        });

        if(getBoardID === undefined){
            callback(new Error('Board not found with the given Board ID'), null);
        }else{
            callback(null, getBoardID);
        }

    }, 2000);
}


module.exports = callback1;