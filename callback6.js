const callback1 = require('./callback1');
const callback2 = require('./callback2');
const callback3 = require('./callback3');


function callback6(boardsData, listsData, cardsData, boardName, callback){

    setTimeout(function(){

        const boardData = boardsData.find(function(board){
            return boardName === board.name;
        });

        if(boardData === undefined){
            callback(new Error('No board data for the given board name', null));
        }else{

            callback1(boardsData, boardData.id, function(err, board){
                if(err){
                    console.log(err);
                }else{
                    console.log(board);

                    callback2(listsData, boardData.id, function(err, lists){
                        if(err){
                            console.log(err);
                        }else{
                            console.log(lists);

                            lists.map(function(list){
                                callback3(cardsData, list.id, function(error, card){
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(card);
                                    }
                                });
                            });
                        }
                    })
                }
            });
        }

    }, 2000);
}



module.exports = callback6;