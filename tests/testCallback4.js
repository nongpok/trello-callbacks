const callback4 = require("../callback4");

const boardsData = require("../data/boards.json");
const listsData = require("../data/lists.json");
const cardsData = require("../data/cards.json");


function callback(err, data){
    if(err){
        console.log(err);
    }else{
        console.log(data);
    }
}


callback4(boardsData, listsData, cardsData, 'Thanos', 'Mind', callback);

// callback4(boardsData, listsData, cardsData, boardName1, listName, callback);

// callback4(boardsData, listsData, cardsData, boardName1, listName, callback);


