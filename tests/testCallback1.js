const callback1 = require('../callback1');
const boardsData = require('../data/boards.json');


function callback(err, data){
    if(err){
        console.log(err);
    }else{
        console.log(data);
    }
}

callback1(boardsData, 'mcu453ed', callback);

callback1(boardsData, 'abcxyz', callback);


