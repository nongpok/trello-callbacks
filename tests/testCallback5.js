const callback5 = require("../callback5");

const boardsData = require("../data/boards.json");
const listsData = require("../data/lists.json");
const cardsData = require("../data/cards.json");


function callback(err, data){
    if(err){
        console.log(err);
    }else{
        console.log(data);
    }
}


callback5(boardsData, listsData, cardsData, 'Thanos', 'Mind', 'Space', callback);



