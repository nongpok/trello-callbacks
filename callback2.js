
function callback2(listsData, boardID, callback){

    setTimeout(function(){

        if(listsData[boardID] === undefined){
            callback(new Error('No list data for the given board ID found'), null);
        }else{
            callback(null, listsData[boardID]);
        }

    }, 2000);
}


module.exports = callback2;