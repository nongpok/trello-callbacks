
function callback3(cardsData, listID, callback){

    setTimeout(function(){

        if(cardsData[listID] === undefined){
            callback(new Error('No card data with the given list ID found'), null);
        }else{
            callback(null, cardsData[listID]);
        }

    }, 2000);
}


module.exports = callback3;